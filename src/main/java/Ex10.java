public class Ex10 {

    public static void main(String[] args) {
        UsdEurCurrencyConverter usdConverter = new UsdEurCurrencyConverter();
        System.out.println("35 EUR = " + usdConverter.fromEuros(35));
        System.out.println("60 USD = " + usdConverter.toEuros(60));

        GbbEurCurrencyConverter gbpConverter = new GbbEurCurrencyConverter();
        System.out.println("35 EUR = " + gbpConverter.fromEuros(35));
        System.out.println("60 GBP = " + gbpConverter.toEuros(60));

    }

    public static abstract class EuroCurrencyConverter {

        public abstract double getExchangeRate();

        public double toEuros(double valueInForeignCurrency) {
            return valueInForeignCurrency / this.getExchangeRate();
        }

        public double fromEuros(double valueInEuros) {
            return valueInEuros * this.getExchangeRate();
        }
    }

    public static class UsdEurCurrencyConverter extends EuroCurrencyConverter {

        @Override
        public double getExchangeRate() {
            return 1.1296d;
        }
    }

    public static class GbbEurCurrencyConverter extends EuroCurrencyConverter {

        @Override
        public double getExchangeRate() {
            return 0.8771d;
        }
    }

}
