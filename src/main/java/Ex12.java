import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

public class Ex12 {

    public static void main(String[] args) {
        System.out.println(multiplyChars("Test", 3));
        System.out.println(censorText("Auto sõidab maanteel, varsti jõuab kohale!"));
//        System.out.println((int)'Õ');
//        System.out.println((int)'Ä');
//        System.out.println((int)'Ö');
//        System.out.println((int)'Ü');
//        System.out.println((int)'õ');
//        System.out.println((int)'ä');
//        System.out.println((int)'ö');
//        System.out.println((int)'ü');

    }

    private static String multiplyChars(String text, int multiplier) {
        String result = "";

        for (char c : text.toCharArray()) {
            for (int i = 0; i < multiplier; i++) {
                result += c;
            }
        }

        return result;
    }

    private static String censorText(String rudeText) {
        String result = "";

//        for (char c : rudeText.toCharArray()) {
//            // Kas tegemist on tähega?
//            int charValue = (int) c;
//
//            int aCharValue = (int) 'a';
//            int zCharValue = (int) 'z';
//            int aCharCapitalValue = (int) 'A';
//            int zCharCapitalValue = (int) 'Z';
//            int c0Value = (int) '0';
//            int c9Value = (int) '9';
//
//            int oTildeCapitalValue = (int) 'Õ';
//            int aUmlCapitalValue = (int) 'Ä';
//            int oUmlCapitalValue = (int) 'Ö';
//            int uUmlCapitalValue = (int) 'Ü';
//            int oTildeValue = (int) 'õ';
//            int aUmlValue = (int) 'ä';
//            int oUmlValue = (int) 'ö';
//            int uUmlValue = (int) 'ü';
//
//            if (
//                    (charValue >= aCharValue && charValue <= zCharValue) ||
//                            (charValue >= aCharCapitalValue && charValue <= zCharCapitalValue) ||
//                            (charValue >= c0Value && charValue <= c9Value) ||
//                            (charValue == oTildeCapitalValue || charValue == aUmlCapitalValue || charValue == oUmlCapitalValue ||
//                                    charValue == uUmlCapitalValue || charValue == oTildeValue || charValue == aUmlValue ||
//                                    charValue == oUmlValue || charValue == uUmlValue)
//            ) {
//                result += "#";
//            } else {
//                result += c;
//            }
//
//        }

//        for (Character c : rudeText.toCharArray()) {
////            if (c.isLetter(c) || c.isDigit(c)) {
////                result += "#";
////            } else {
////                result += c;
////            }
//
//            result += (c.isLetter(c) || c.isDigit(c)) ? "#" : c;
//        }

//        return result;

        return rudeText.chars()
                .mapToObj(c -> (Character.isDigit(c) || Character.isLetter(c)) ? "#" : String.valueOf((char)c))
                .collect(Collectors.joining(""));
    }

}
