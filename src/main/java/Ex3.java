public class Ex3 {

    public static void main(String[] args) {
        float bmi = calculateBodyMassIndex(179, 75);
        String bmiText = getBmiText(bmi);
        System.out.println("BMI: " + bmi + " / " + bmiText);
    }

    private static String getBmiText(float bmi) {
        if (bmi < 16) {
            return "Tervisele ohtlik alakaal";
        } else if (bmi < 19) {
            return "Alakaal";
        } else if (bmi < 25) {
            return "Normaalkaal";
        } else if (bmi < 30) {
            return "Ülekaal";
        } else if (bmi < 35) {
            return "Rasvumine";
        } else if (bmi < 40){
            return  "Tugev rasvumine";
        } else {
            return "Tervisele ohtlik rasvumine";
        }
    }

    private static float calculateBodyMassIndex(int lengthInCm, float weightInKg) {
//        float lengthInM;
//        if (lengthInCm > 0) {
//            lengthInM = lengthInCm / 100;
//        }

        float lengthInM = lengthInCm / 100.0f;
        return lengthInCm > 0 ? weightInKg / (lengthInM * lengthInM) : 0;
    }


}
