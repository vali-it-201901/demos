public class Ex7 {

    public static void main(String[] args) {
        Period period = new Period(1_000_000_000);
        System.out.println("Perioodi pikkus: " + period);
    }

    public static class Period {

        private int periodInSeconds;
        private int years;
        private int months;
        private int days;
        private int hours;
        private int minutes;
        private int seconds;

        public Period(int periodInSeconds) {
            this.periodInSeconds = periodInSeconds;

            years = this.periodInSeconds / (365 * 24 * 60 * 60);
            int leftover = this.periodInSeconds % (365 * 24 * 60 * 60);

            months = leftover / (30 * 24 * 60 * 60);
            leftover = leftover % (30 * 24 * 60 * 60);

            days = leftover / (24 * 60 * 60);
            leftover = leftover % (24 * 60 * 60);

            hours = leftover / (60 * 60);
            leftover = leftover % (60 * 60);

            minutes = leftover / 60;
            leftover = leftover % 60;

            seconds = leftover;
        }

        @Override
        public String toString() {
            return String.format("Periood %s sekundit: (%s aastat, %s kuud, %s päeva, %s tundi, %s minutit, %s sekundit)",
                    periodInSeconds, years, months, days, hours, minutes, seconds);
        }
    }
}
