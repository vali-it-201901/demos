import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex1 {
    private static final double VAT_PERCENTAGE = 20;

    public static void main(String[] args) {
//        double priveWithoutVat = stripVat(546.5);
//        System.out.println(priveWithoutVat);

//        System.out.println(isPalindrome("Aias sadas saia"));
        System.out.println(isPalindrome2.apply("Aias sadas saiaS"));

//        List<String> result = Arrays.asList("test".toCharArray()).stream().map(c -> c).collect(Collectors.toList());
//        System.out.println(result);

//        List<String> result = Arrays.asList("test".toCharArray()).stream().map(c -> "C").collect(Collectors.toList());
//         //.map(c -> System.out.println(c));
//        List<String> characterList = "test".chars().mapToObj(c -> new StringBuilder().append((char)c).append((char)c).append((char)c).toString()).collect(Collectors.toList());
//        System.out.println(characterList);
    }

    private static double stripVat(double priceWithVat) {
        double priceWithoutVat = priceWithVat / (100 + VAT_PERCENTAGE) * 100;
        priceWithoutVat = Math.round(priceWithoutVat * 100) / 100.0;
        return priceWithoutVat;
    }

    private static boolean isPalindrome(String text) {
        String textReversed = "";
//        for(int i = 0; i < text.length(); i++) {
//            textReversed = text.charAt(i) + textReversed;
//        }

//        for (char c : text.toCharArray()) {
//            textReversed = c + textReversed;
//        }

        textReversed = new StringBuffer(text).reverse().toString();

        return text.equalsIgnoreCase(textReversed);
    }

    private static Function<String, Boolean> isPalindrome2 =
            s -> new StringBuffer(s).reverse().toString().equalsIgnoreCase(s);

}
